import React from 'react';
import '../styles/styles.scss';
import Form from './Pages/Form'
import Courses from './Pages/Courses';
import {BrowserRouter as Router, Route, Switch } from "react-router-dom"
import Course from "./Pages/Course"
import MainMenu from "./Organisms/MainMenu"
import History from "./Pages/History"
import Home from './Pages/Home';
import Users from './Pages/Users';

const App = () => (
  <Router>
    <MainMenu />
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/CourseCard/:id" component={Course} />
      <Route path="/CourseCard" component={Courses} />
      <Route path="/History" component={History} />
      <Route path="/user" component={Users} />
      <Route path="/form" component={() => <Form name="ruta" />} />
      <Route component={() => (
        <div className="ed-grid">
          <h1>Error 404</h1>
          <span>Pagina no encontrado</span>
        </div>
      )} />
    </Switch>
  </Router>
)

export default App;


//Reglas jsx:
// los elmentos jsx solo pueden tener una etiqueta. y dentro de esa etiqueta se puede agregar cualquier cosa
// 1: Toda las etiquetas deben cerrarse
// 2: los componentes deben de devolver una sola etiqueta
// 3: apoyarse de los frgment cuando necesito devolver mas de un elemnto
// 4: fragment => <> </>
// 5: Las etiquetas img siempre se cierran a comparacion de html
// 6: class pasa a ser className
// 7: for => htmlFor.
// 8: no if,else,while.. Se utiliza -> ? :
