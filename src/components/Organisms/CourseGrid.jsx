import React from "react"
import CourseCard from "../Molecules/CourseCard"
import withLoader from "../HOC/withLoader"

const CourseGrid = ({courses}) => (
    <div className="ed-grid m-grid-4">
        {
            courses.map(cursoIterador => (
                <CourseCard
                    id={cursoIterador.id}
                    title={cursoIterador.titulo}
                    image={cursoIterador.image}
                    price={cursoIterador.price}
                    professor={cursoIterador.professor}
                />
            ))
        }
    </div>
)

export default withLoader("courses")(CourseGrid) 