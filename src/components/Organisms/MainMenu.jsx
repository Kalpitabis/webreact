import React from "react"
import {NavLink} from "react-router-dom"

const MainMenu = () => (
    <header className="main-header">
        <div className="ed-grid s-grid-5 lg-grid-4">
            <div className="s-cols-4 lg-cols-1 s-cross-center">
                <a href="/">
                    <img className="main-logo" src="https://st3.depositphotos.com/1768926/18385/v/1600/depositphotos_183855844-stock-illustration-v-letter-logo-business-template.jpg" alt="logo principal"/>
                </a>
            </div>
            <div className="s-grid-1 lg-cols-3 s-cross-center s-main-end header-links">
                <nav className="main-menu" id="main-menu">
                    <ul>
                        <li><NavLink exact to="/" activeClassName="activo"> Inicio </NavLink></li>
                        <li><NavLink to="/CourseCard" activeClassName="activo"> Cursos </NavLink></li>
                        <li><NavLink to="/form" activeClassName="activo"> Formulario </NavLink></li>
                        <li><NavLink to="/user" activeClassName="activo"> user </NavLink></li>
                    </ul>
                </nav>
                <div className="main-menu-toggle to-l" id="main-menu-toggle"></div>
            </div>
        </div>
    </header>
)

export default MainMenu