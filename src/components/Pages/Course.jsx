import React from "react"

const cursos = [
    {
        "id": 1,
        "titulo": "React desde cero",
        "image": "https://drupal.ed.team/sites/default/files/styles/16_9_medium/public/imagenes-cdn-edteam/2019-11/js-poo.png",
        "price": 30,
        "profesor": "Vadlemar"
    },
    {
        "id": 2,
        "titulo": "go desde cero",
        "image": "https://images.unsplash.com/photo-1480714378408-67cf0d13bc1b?ixlib=rb-1.2.1&w=1000&q=80",
        "price": 20,
        "profesor": "Mario"
    },
    {
        "id": 3,
        "titulo": "lol desde cero",
        "image": "https://i.ytimg.com/vi/NmfKKZ-ZXTg/maxresdefault.jpg",
        "price": 10,
        "profesor": "Sergio"
    }
]
const Course = ({match}) => {

    const cursoActual = cursos.filter(c => c.id === parseInt(match.params.id))[0]

    return (
        cursoActual ? (
            <>
                <h1 className="m-cols-3"> {cursoActual.titulo} </h1>
                <img className="m-cols-1" src={cursoActual.image} alt="Imagen del curso" />
                <p className="m-cols-2">profesor: {cursoActual.profesor} </p>
            </>
        ) : <h1>El curso no existe</h1>
    )
}

export default Course